#prerequest: python3, a proxy application since you know why
#1. set your password in inoreader Profile
#2. curl https://www.inoreader.com/accounts/ClientLogin -d Email=username -d Passwd=password --verbose
#3. you can see Auth like this Auth=G2UlCa...Fx
#4. replace G2UlCa...Fx in source code with your Auth
#5. do not leak your Auth, it will not change even you change your password.
#6. have fun

import urllib.request
import json
import hashlib
import sqlite3

auth = 'G2UlCa...Fx'
feed = 'http://www.cnbeta.com/backend.php'
blacklist = {'大理由':True, '库克':True}
http_proxy = ""

def is_forbid(text, blacklist):
    for w in blacklist:
        if w in text:
            return True
    return False

def open_db():
    conn = sqlite3.connect('ids.db')
    c = conn.cursor()
    exist = c.execute('select * from sqlite_master where name = ?',('ids',)).fetchone()
    if exist == None:
        c.execute('create table ids (hash char(40) primary key, id char(48))')
        conn.commit()
    return conn


def query_db(conn, hash):
    c = conn.cursor()
    c.execute("SELECT * FROM ids WHERE hash = ? ", (hash,))
    return c.fetchone()


def insert_to_db(conn, item_id, digest):
    c = conn.cursor()
    c.execute("INSERT INTO ids VALUES (?, ?)", (digest,item_id))
    conn.commit()


def is_duplicate(conn, item_id, title, content):
    ret = False
    sha1 = hashlib.sha1()
    sha1.update(title.encode('utf8'))
    sha1.update(content.encode('utf8'))
    digest = sha1.hexdigest()
    row = query_db(conn,digest)
    if row == None:
        insert_to_db(conn, item_id, digest)
    elif row[1] != item_id:
        ret = True
    return ret


def url_open(url):
    req = urllib.request.Request(url)
    print (auth)
    req.add_header('Authorization', auth)
    if len(http_proxy) > 0:
        req.set_proxy(http_proxy,'http')
    r = urllib.request.urlopen(req)
    return r

def mark_as_read(toreads):
    if len(toreads):
        url = 'https://www.inoreader.com/reader/api/0/edit-tag?r=user/-/state/com.google/read'
        for toread in toreads:
            url = url + "&i=" + toread
        print(url)
        r = url_open(url)

count=0
def hit(text):
    global count
    count += 1
    try:
        print('['+str(count)+']: '+text)
    except:
        print("can't decode string")

try:
    config = json.load(open("config.json",  encoding='utf8'))
    if 'auth' in config:
        auth = config['auth']
    if 'feed' in config:
        feed = config['feed']
    if 'blacklist' in config:
        l = config['blacklist']
        for w in l:
            print(w)
            blacklist[w] = True
    if ('http_proxy' in config):
        http_proxy = config['http_proxy']
except:
    print("no config file")

auth = 'GoogleLogin auth=' + auth
conn = open_db()
cont = ''
try:
    while True:
        url = 'https://www.inoreader.com/reader/atom/feed/' + feed + '?output=json&xt=user/-/state/com.google/read' + cont
        print(url)
        r = url_open(url)
        p = json.loads(r.read().decode('utf8'))
        if not 'items' in p:
            print('no items in p')
            break
        toreads = []
        for item in p['items']:
            item_id  = item['id']
            title = item['title']
            content = item['summary']['content']
            if is_forbid(title,blacklist) or is_duplicate(conn, item_id, title, content):
                toreads.append(item_id)
                hit(title)
        mark_as_read(toreads)
        if not 'continuation' in p:
            break
        cont = '&c='+p['continuation']
finally:
    conn.close()

